using GraphQL;
using GraphQL.Types;
using System.Linq;
using BackendFP.GraphQL.Types;
using BackendFP.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace BackendFP.GraphQL
{
    class ProjectQuery : ObjectGraphType
    {
        private readonly DatabaseContext _context;
        public ProjectQuery(DatabaseContext context)
        {
            _context = context;
            
            Field<ProyectType>("proyects", 
                arguments: new QueryArguments(new QueryArgument<IdGraphType> { Name = "id" }),
                resolve: context => {
                    return _context.Proyects.Find(context.GetArgument<long>("id"));
                }
            );
            Field<ListGraphType<UserType>>("users", resolve: context => {
                return _context.Users.ToListAsync();
            });

            Field<ListGraphType<UserType>>("users",
                arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "email" }),
                resolve: context => {
                    var results = from users in _context.Users select users;
                    if(context.HasArgument("email")){
                        var email = context.GetArgument<string>("email");
                        results = results.Where(c => c.Email.Equals(email));
                    }
                    return results;
                }
            );
        }
    }
}