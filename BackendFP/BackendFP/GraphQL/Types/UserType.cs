using GraphQL.Types;
using BackendFP.Models;

namespace BackendFP.GraphQL.Types
{
    public class UserType : ObjectGraphType<User>
    {
        public UserType()
        {
            Name = "User";
            Field(x => x.Id);
            Field(x => x.Name);
            Field(x => x.LastName);
            Field(x => x.Email);
            Field(x => x.Password);
        }
    }
}