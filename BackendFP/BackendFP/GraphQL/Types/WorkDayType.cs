using GraphQL.Types;
using BackendFP.Models;

namespace BackendFP.GraphQL.Types
{
    public class WorkDayType : ObjectGraphType<WorkDay>
    {
        public WorkDayType()
        {
            Name = "WorkDay";
            Field(x => x.Id);
            Field(x => x.Hours);
            Field(x => x.Description);
        }
    }
}