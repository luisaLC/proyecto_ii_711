using GraphQL.Types;
using BackendFP.Models;

namespace BackendFP.GraphQL.Types
{
    public class TeamType : ObjectGraphType<Team>
    {
        public TeamType()
        {
            Name = "Team";
            Field(x => x.Id);
            Field(x => x.Name);
            Field(x => x.Lastname);
        }
    }
}