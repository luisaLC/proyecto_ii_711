using GraphQL.Types;
using BackendFP.Models;

namespace BackendFP.GraphQL.Types
{
    public class ProyectType : ObjectGraphType<Proyect>
    {
        public ProyectType()
        {
            Name = "Proyect";
            Field(x => x.Id);
            Field(x => x.Description);
            Field(x => x.Tittle);
            Field(x => x.Team);
        }
    }
}