using GraphQL;
using GraphQL.Types;


namespace BackendFP.GraphQL
{
    class ProjectSchema : Schema
    {
        public ProjectSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<ProjectQuery>();
            /*
            Query
            Mutation
            Subcription *
            */
        }
    }
}