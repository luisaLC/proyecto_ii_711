using Microsoft.EntityFrameworkCore;

namespace BackendFP.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            
        }

        public DbSet<Proyect> Proyects { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<WorkDay> WorkDays { get; set; }
    }
}