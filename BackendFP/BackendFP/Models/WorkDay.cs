using System.Collections.Generic;

namespace BackendFP.Models
{
    public class WorkDay
    {
        public long Id { get; set; }
        public string Hours { get; set; }
        public string Description { get; set; }
        
    }
}