using BackendFP.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BackendFP.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class ProyectsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public ProyectsController(DatabaseContext context)
        {
            _context = context;
        }

        [HttpGet]
        // https://localhost:5001/api/branches
        public async Task<ActionResult<IEnumerable<Proyect>>> GetProyects()
        {
            return await _context.Proyects.ToListAsync();
        }

        [HttpGet("{id}")]
        // https://localhost:5001/api/branches/1
        public async Task<ActionResult<Proyect>> GetProyect(long id)
        {
            var proyect = await _context.Proyects.FindAsync(id);
            if (proyect == null) {
                return NotFound();
            }
            return proyect;
        }

        [HttpPost]
        // https://localhost:5001/api/branches
        public async Task<ActionResult<User>> PostProyect(User user){
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetUser", new { id = user.Id }, user);
        }

        [HttpDelete("{id}")]
        // https://localhost:5001/api/branches/1
        public async Task<ActionResult<Proyect>> DeleteProyect(long id){
            var proyect = await _context.Proyects.FindAsync(id);
            if (proyect == null) {
                return NotFound();
            }

            _context.Proyects.Remove(proyect);
            await _context.SaveChangesAsync();
            return proyect;
        }

        [HttpPut("{id}")]
        // https://localhost:5001/api/branches/1
        public async Task<ActionResult<Proyect>> Updatev(long id, Proyect proyect){
            if (id != proyect.Id) {
                return BadRequest();
            }
            _context.Entry(proyect).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetProyect", new { id = proyect.Id }, proyect);
        }
    }
}