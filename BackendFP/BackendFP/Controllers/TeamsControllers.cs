using BackendFP.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BackendFP.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class TeamsControllers : ControllerBase
    {
        private readonly DatabaseContext _context;

        public TeamsControllers(DatabaseContext context)
        {
            _context = context;
        }

        [HttpGet]
        // https://localhost:5001/api/branches
        public async Task<ActionResult<IEnumerable<Team>>> GetTeams()
        {
            return await _context.Teams.ToListAsync();
        }

        [HttpGet("{id}")]
        // https://localhost:5001/api/branches/1
        public async Task<ActionResult<Team>> GetTeam(long id)
        {
            var team = await _context.Teams.FindAsync(id);
            if (team == null) {
                return NotFound();
            }
            return team;
        }

        [HttpPost]
        // https://localhost:5001/api/branches
        public async Task<ActionResult<Team>> PostTeam(Team team){
            _context.Teams.Add(team);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetTeam", new { id = team.Id }, team);
        }

        [HttpDelete("{id}")]
        // https://localhost:5001/api/branches/1
        public async Task<ActionResult<Team>> DeleteTeam(long id){
            var team = await _context.Teams.FindAsync(id);
            if (team == null) {
                return NotFound();
            }

            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
            return team;
        }

        [HttpPut("{id}")]
        // https://localhost:5001/api/branches/1
        public async Task<ActionResult<Team>> Updatev(long id, Team team){
            if (id != team.Id) {
                return BadRequest();
            }
            _context.Entry(team).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetTeam", new { id = team.Id }, team);
        }
    }
}