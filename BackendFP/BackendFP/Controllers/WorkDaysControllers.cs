using BackendFP.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BackendFP.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class WorkDaysControllers : ControllerBase
    {
        private readonly DatabaseContext _context;

        public WorkDaysControllers(DatabaseContext context)
        {
            _context = context;
        }

        [HttpGet]
        // https://localhost:5001/api/branches
        public async Task<ActionResult<IEnumerable<WorkDay>>> GetWorkDays()
        {
            return await _context.WorkDays.ToListAsync();
        }

        [HttpGet("{id}")]
        // https://localhost:5001/api/branches/1
        public async Task<ActionResult<WorkDay>> GetWorkDay(long id)
        {
            var workday = await _context.WorkDays.FindAsync(id);
            if (workday == null) {
                return NotFound();
            }
            return workday;
        }

        [HttpPost]
        // https://localhost:5001/api/branches
        public async Task<ActionResult<WorkDay>> PostTeam(WorkDay workday){
            _context.WorkDays.Add(workday);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetWorkDay", new { id = workday.Id }, workday);
        }

        [HttpDelete("{id}")]
        // https://localhost:5001/api/branches/1
        public async Task<ActionResult<WorkDay>> DeleteWorkDay(long id){
            var workday = await _context.WorkDays.FindAsync(id);
            if (workday == null) {
                return NotFound();
            }

            _context.WorkDays.Remove(workday);
            await _context.SaveChangesAsync();
            return workday;
        }

        [HttpPut("{id}")]
        // https://localhost:5001/api/branches/1
        public async Task<ActionResult<WorkDay>> Updatev(long id, WorkDay workday){
            if (id != workday.Id) {
                return BadRequest();
            }
            _context.Entry(workday).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetWorkDay", new { id = workday.Id }, workday);
        }
    }
}