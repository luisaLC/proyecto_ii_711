import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { AngularMaterialModule } from './angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatCardModule } from '@angular/material/card';
import { ProjectComponent, ProjectForm, ProjectEdit } from './project/project.component';
import { BoardComponent, TaskStatusForm, TaskForm} from './board/board.component';

import { TaskComponent, TaskEdit, TaskDelete } from './task/task.component';
import { TaskStatusComponent, TaskStatusEdit } from './task-status/task-status.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    TaskStatusForm,
    AppComponent,
    ProjectComponent,
    ProjectForm,
    ProjectEdit,
    BoardComponent,
    TaskComponent,
    TaskEdit,
    TaskForm,
    TaskDelete,
    TaskStatusComponent,
    TaskStatusEdit,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DragDropModule,
    MatCardModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
