import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public user = {
    name: '',
    lastname: '',
    email: '',
    password: '',
    confirm_password: '',
    phone: ''
  }
  public error = null;
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  register() {
    !this.handle_error() &&
      this.router.navigate(['/project']);
  }

  handle_error() {
    if (!this.user.name || !this.user.lastname ||
      !this.user.email || !this.user.password ||
      !this.user.confirm_password || !this.user.phone
    )
      this.error = "required data";
    else if (this.user.password !== this.user.confirm_password)
      this.error = "incorrect passwords";
    else return false;
    return true;
  }
}
