import { Component, OnInit, Inject } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

//Interface taskStatusData
export interface taskStatusData {
  name: string;
}

//Variables para crear los datos
export interface TaskCreateData {
  title: string;
  status: string;
  description: string;
  user: string;
}

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})


export class BoardComponent implements OnInit {

  //TASK STATUS
  name: string;

  //TASK
  title: string;
  status: string;
  description: string;
  user: string;

  constructor(public dialog: MatDialog) { }

  
  //Para agregar los datos
  openDialogAddTask() {
    const dialogRef = this.dialog.open(TaskForm,
      {
        width: '300px',
        data: { title: this.title, status: this.status, description: this.description, user: this.user }
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  ngOnInit(): void {
  }

  //Lista de tareas x estado
  public boardList = [
    {
      id: '1',
      title: 'To do',
      task: [
        { id: '1', name: 'Get to work'},
        { id: '2', name: 'Pick up groceries'},
        { id: '3', name: 'Go home'},
        { id: '4', name: 'Fall asleep'},
      ]
    },
    {
      id: '2',
      title: 'Done',
      task: [
        { id: '5', name: 'Get up'},
        { id: '6', name: 'Brush teeth'},
        { id: '7', name: 'Take a shower'},
        { id: '8', name: 'Check e-mail'},
        { id: '9', name: 'Walk dog'},
      ]
    },
    {
      id: '3',
      title: 'QA',
      task: [
        { id: '10', name: 'Get upaaaaa'},
        { id: '11', name: 'Brush teethaaaaaa'},
        { id: '12', name: 'Take a shower111'},
        { id: '13', name: 'Check e-mail2342'},
        { id: '14', name: 'Walk 23423423'},
      ]
    },
  ]

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  get boardIds(): string[] {
    return this.boardList.map(board => board.id);
  }

  //Para abrir ventana flotante para agregar task status
  openDialogAddTaskStatus() {
    const dialogRef = this.dialog.open(TaskStatusForm,
      {
        width: '300px',
        data: { name: this.name }
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  
}

@Component({
  selector: 'task-status-form',
  templateUrl: 'task-status-form.html',
})

//para exportar la clase
export class TaskStatusForm {
  constructor(
    public dialogRef: MatDialogRef<TaskStatusForm>,
    @Inject(MAT_DIALOG_DATA) public taskStatus: taskStatusData) { }

    create_task_status(): void {
      console.log("SUBMIT")
    }
}


//TASK-FORM.HTML
@Component({
  selector: 'task-form',
  templateUrl: 'task-form.html',
})

export class TaskForm {
  constructor(
    public dialogRef: MatDialogRef<TaskForm>,
    @Inject(MAT_DIALOG_DATA) public task: TaskCreateData) { }

    create_task(): void {
      console.log("SUBMIT")
    }
}

