import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user = {
    email: '',
    password: ''
  }

  public error = null;
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login() {
    !this.handle_error() &&
      this.router.navigate(['/project']);
  }

  handle_error() {
    if (!this.user.email || !this.user.password)
      this.error = "required data";
    else return false;
    return true
  }

}
