import { Component, Input, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';



//Variables para actualizar los datos
export interface TaskUpdateData {
  title: string;
  status: string;
  description: string;
  user: string;
}
//Variables para borrar los datos
export interface TaskDeleteData {
  title: string;
  status: string;
  description: string;
  user: string;
}

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  @Input() task: any;

  title: string;
  status: string;
  description: string;
  user: string;

  constructor(
    public dialog: MatDialog, 
    private router: Router) { }



  //Para actualizar los datos
  openDialogUpdate() {
    const dialogRef = this.dialog.open(TaskEdit,
      {
        width: '300px',
        data: { title: this.title, status: this.status, description: this.description, user: this.user }
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  //Para borrar los datos
  openDialogDelete() {
    const dialogRef = this.dialog.open(TaskDelete,
      {
        width: '300px',
        data: { title: this.title, status: this.status, description: this.description, user: this.user }
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  ngOnInit(): void {
  }

}



//TASK-EDIT.HTML
@Component({
  selector: 'task-edit',
  templateUrl: 'task-edit.html',
})

export class TaskEdit {
  constructor(
    public dialogRef: MatDialogRef<TaskEdit>,
    @Inject(MAT_DIALOG_DATA) public task: TaskUpdateData) { }

    update_task(): void {
      console.log("SUBMIT")
    }
}

//TASK-DELETE.HTML
@Component({
  selector: 'task-delete',
  templateUrl: 'task-delete.html',
})

export class TaskDelete {
  constructor(
    public dialogRef: MatDialogRef<TaskDelete>,
    @Inject(MAT_DIALOG_DATA) public task: TaskDeleteData) { }

    delete_task(): void {
      console.log("SUBMIT")
    }
}
