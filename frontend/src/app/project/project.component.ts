import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

//Variables para crear los datos
export interface projectCreateData {
  title: string;
  description: string;
}

//Variables para actualizar los datos
export interface projectUpdateData {
  title: string;
  description: string;
}

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})

export class ProjectComponent implements OnInit {

  title: string;
  description: string;

  //"Aca irían los proyectos"
  public projects = [
    {
      title: "Angular",
      description:
        "Accessibility Angular Material uses native <button> and <a> elements to ensure an accessible experience by default. The <button> element should be used for any interaction that performs an action on the current page. The <a> element should be used for any interaction that navigates to another view."
    },
    {
      title: "Material",
      description:
        "Buttons can be colored in terms of the current theme using the color property to set the background color to primary, accent, or warn."
    }
  ];

    
    constructor(
      public dialog: MatDialog, 
      private router: Router) { }
  
  //Para agregar los datos
  openDialogAdd() {
    const dialogRef = this.dialog.open(ProjectForm,
      {
        width: '300px',
        data: { title: this.title, description: this.description }
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  //Para actualizar los datos
  openDialogUpdate() {
    const dialogRef = this.dialog.open(ProjectEdit,
      {
        width: '300px',
        data: { title: this.title, description: this.description }
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  //redirecciona
  projectNavigate(){
    this.router.navigate(['/board']);
  }


  ngOnInit(): void {
  }
}

//PROJECT-FORM.HTML
@Component({
  selector: 'project-form',
  templateUrl: 'project-form.html',
})

export class ProjectForm {
  constructor(
    public dialogRef: MatDialogRef<ProjectForm>,
    @Inject(MAT_DIALOG_DATA) public project: projectCreateData) { }

    create_project(): void {
      console.log("SUBMIT")
    }
}

//PROJECT-EDIT.HTML
@Component({
  selector: 'project-edit',
  templateUrl: 'project-edit.html',
})

export class ProjectEdit {
  constructor(
    public dialogRef: MatDialogRef<ProjectEdit>,
    @Inject(MAT_DIALOG_DATA) public project: projectUpdateData) { }

    update_project(): void {
      console.log("SUBMIT")
    }
}