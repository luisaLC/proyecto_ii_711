import { Component, Input, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

//Variables para actualizar los datos
export interface TaskStatusUpdateData {
  title: string;
}

@Component({
  selector: 'app-task-status',
  templateUrl: './task-status.component.html',
  styleUrls: ['./task-status.component.css']
})
export class TaskStatusComponent implements OnInit {

  @Input() status: any;
  
  title: string;

  constructor(
    public dialog: MatDialog, 
    private router: Router) { }


   //Para actualizar los datos
   openDialogUpdate() {
    const dialogRef = this.dialog.open(TaskStatusEdit,
      {
        width: '300px',
        data: { title: this.title}
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
    
  ngOnInit(): void {
  }

}

//TASK-STATUS-EDIT.HTML
@Component({
  selector: 'task-status-edit',
  templateUrl: 'task-status-edit.html',
})

export class TaskStatusEdit {
  constructor(
    public dialogRef: MatDialogRef<TaskStatusEdit>,
    @Inject(MAT_DIALOG_DATA) public status: TaskStatusUpdateData) { }

    update_task_status(): void {
      console.log("SUBMIT")
    }
}

